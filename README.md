## Libyuv

## Introduction

libyuv is Google's Open-Source Library for conversion, rotation and scaling between YUV and RGB. It supports compiling and execution on Windows, Linux, Mac, and other platforms, x86, x64, arm architectures, and SIMD instruction acceleration such as SSE, AVX, NEON.

### Features

* Converting ARGB image to RGBA

## Usage Instructions
Can call the api for the conversion of ARGB image to RGBA
Libyuv.ARGBToRGBA(byteArray, w*4,  rgbbuffer, w*4, w, h);



## Installation tutorial
```
method 1: Generate har package from library add it to the libs folder
		add the following code to gradle of entry 
		
		implementation fileTree(dir: 'libs', include: ['*.har'])

method 2:
allprojects {
    repositories {
        mavenCentral()
    }
}

dependencies {
    implementation 'io.openharmony.tpc.thirdlib:libyuv:1.0.2'
}
```

