# Libyuv library

libyuv is Google's Open-Source Library for conversion, rotation and scaling between YUV and RGB. It supports compiling and execution on Windows, Linux, Mac, and other platforms, x86, x64, arm architectures, and SIMD instruction acceleration such as SSE, AVX, NEON.

# Features

* Converting ARGB image to RGBA

Visit [Libyuv website](https://github.com/bilibili/libyuv) 


## Installation tutorial:
Installation tutorial
local .so file generation to be included with sample.
a) Add all the Libyuv related native source files (.cpp & .h) to src/main/cpp directory of the entry.
b) Create CMakeLists.txt including all the source files to be built to finally create liblibyuv.so shared library.
c) Add the following native build configuration to Gradle, for the gradle to identify CMakeLists.txt and build the native sources for specific instruction set,

externalNativeBuild {
        path "src/main/cpp/CMakeLists.txt"
        arguments "-v"
        abiFilters "arm64-v8a"
    }

Usage Instructions
For a working implementation of this project see the entry/ folder.

