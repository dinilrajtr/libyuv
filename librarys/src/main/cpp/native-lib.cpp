//
// Created by DELL on 2018/3/26.
//

#include <jni.h>
#include <string>
#include "libyuv/libyuv.h"

extern "C" {



JNIEXPORT
JNICALL
void Java_com_huawei_libyuv_Libyuv_RGBAToARGB(
        JNIEnv *env, jclass *jcls,
        jbyteArray srcBuffer,jint src_stride_frame,
        jbyteArray dstBuffer, jint dst_stride_argb,
        jint width, jint height) {

    uint8_t* src_frame = (uint8_t*) env->GetByteArrayElements(srcBuffer, 0);
    uint8_t* dst_argb = (uint8_t*) env->GetByteArrayElements(dstBuffer, 0);

    libyuv::RGBAToARGB(src_frame,src_stride_frame, dst_argb,dst_stride_argb, width, height);

    //remember release
    env->ReleaseByteArrayElements(srcBuffer, (jbyte*)src_frame, 0);
    env->ReleaseByteArrayElements(dstBuffer, (jbyte*)dst_argb, 0);
}

JNIEXPORT
JNICALL
void Java_com_huawei_libyuv_Libyuv_NV21ToARGB(
JNIEnv *env, jclass *jcls,
jbyteArray yBuffer,jint y_stride,
jbyteArray uvBuffer,jint uv_stride,
jbyteArray dstARGB,jint dst_stride_argb,
jint width, jint height) {

uint8_t* src_y=(uint8_t*) env->GetByteArrayElements(yBuffer, 0);
uint8_t* src_uv=(uint8_t*) env->GetByteArrayElements(uvBuffer, 0);
uint8_t* dst_argb = (uint8_t*) env->GetByteArrayElements(dstARGB, 0);

libyuv::NV21ToARGB(src_y, y_stride, src_uv, uv_stride, dst_argb, dst_stride_argb, width, height);

//remember release
env->ReleaseByteArrayElements(dstARGB, (jbyte*)dst_argb, 0);
env->ReleaseByteArrayElements(yBuffer, (jbyte*)src_y, 0);
env->ReleaseByteArrayElements(uvBuffer, (jbyte*)src_uv, 0);

}

JNIEXPORT
JNICALL
void Java_com_huawei_libyuv_Libyuv_ARGBToRGBA(
        JNIEnv *env, jclass *jcls,
        jbyteArray yBuffer,jint y_stride,
        jbyteArray dstRGBA,jint dst_stride_rgba,
        int width, int height) {

    uint8_t* src_y=(uint8_t*) env->GetByteArrayElements(yBuffer, 0);
    uint8_t* dst_rgba = (uint8_t*) env->GetByteArrayElements(dstRGBA, 0);

    libyuv::ARGBToRGBA(src_y, y_stride, dst_rgba, dst_stride_rgba, width, height);

    //remember release
    env->ReleaseByteArrayElements(dstRGBA, (jbyte*)dst_rgba, 0);
    env->ReleaseByteArrayElements(yBuffer, (jbyte*)src_y, 0);
}
}
