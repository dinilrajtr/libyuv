/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.huawei.libyuv;

public class Libyuv {
    static {
   //     System.loadLibrary("native-lib");
        System.loadLibrary("libyuv");
    }
    public native static void RGBAToARGB(byte[] srcBuffer,int src_stride_frame,
                                         byte[] dstBuffer, int dst_stride_argb,
                                         int width, int height);
    public native static void NV21ToARGB(byte[] yBuffer,int y_stride,
                                         byte[] uvBuffer,int uv_stride,
                                         byte[] dstARGB,int dst_stride_argb,
                                         int width, int height);

    public native static void ARGBToRGBA(byte[] yBuffer,int y_stride,
                                         byte[] dstRGBA,int dst_stride_rgba,
                                         int width, int height);
    public native static byte[] ConvertToI420(byte[] nv21, int w, int h, int type);
}
